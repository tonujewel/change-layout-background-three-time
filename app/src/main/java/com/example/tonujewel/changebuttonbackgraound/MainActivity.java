package com.example.tonujewel.changebuttonbackgraound;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView submit;
    RelativeLayout activity_main;
    int i = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit = (TextView)findViewById(R.id.submit);
        activity_main = (RelativeLayout) findViewById(R.id.activity_main);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (i==1){
                    activity_main.setBackgroundColor(getResources().getColor(R.color.one));
                    i = 2;
                }else if(i==2){
                    activity_main.setBackgroundColor(getResources().getColor(R.color.two));
                    i = 3;
                }else {
                    activity_main.setBackgroundResource(R.drawable.jewel);
                    i=1;
                }
            }
        });
    }
}
